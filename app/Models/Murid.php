<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Murid extends Model
{
   	protected $table = "Murid";
   	protected $fillable = ["id", "firstname", "lastname", "created_at", "updated_at"];
   	protected $primaryKey = "id";
}
