<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tugas extends Model
{
    protected $table = "tugas";
    protected $fillable = ["id", "name", "created_at", "updated_at"];
    protected $primaryKey = "id";
}
