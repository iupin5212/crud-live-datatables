<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use App\Models\Murid;
use App\Models\Tugas;
use DB;

class DatatablesController extends Controller
{
    public function index()
    {
    	return view("dt-send.index");
    }

    public function data()
    {
    	return Datatables::of(Tugas::select("id", "name"))->addColumn("action", function($x) {

    			$datatables = Murid::all();

    			ob_start();
            	echo '<select class="form-control select2-multi" name="nama">';
            	echo '<option value=""> <b>Pilih Murid</b></option>';
			        foreach ($datatables as $value){
			            echo '<option value="'.$value->id.'">';
			            echo $value->firstname;
			            echo '</option>"';  
			        } 
			    echo '</select>'; 
				$output = ob_get_contents();
				ob_end_clean();

			return $output;

    	})->editColumn('id', '{{$id}}')->make(true); // note: u can use AddIndexColumn() for auto numbering
    }

    public function send(Request $req, $id)
    {
    	if ($req->isMethod("POST")) {
    		$data = $req->all();
    		if ($data == null) {
    			$alert = [
    				"message" => "Harap Pilih Murid Terlebih Dahulu",
    				"alert-type" => "info"
    			];
    		} elseif ($data != null) {
    			$xx = Tugas::find($id);
    			$x = Murid::where("id", $req->is_nama)->first();
    			$alert = [
    				"message" => "Kamu Memilih Murid ".$x->firstname." Untuk Tugas ".$xx->name." Dan Deskripsi ".$req->deskripsi."",
    				"alert-type" => "success"
    			];
    			return redirect()->route("dt_send.index")->with($alert);
    		}

    	}
    }

    public function index2()
    {
    	return view("dt-crud.index");
    }

    public function data2()
    {
        DB::statement(DB::raw("set @rownum = 0"));
    	return Datatables::of(Murid::select(DB::raw("@rownum := @rownum + 1 as rownum"), "id", "firstname", "lastname"))
    		->addColumn("action", function($x) {
    			return '<button class="btn btn-warning waves-effect" id="edit"><i class="material-icons">edit</i></button> <button class="btn btn-danger waves-effect" id="delete"><i class="material-icons">delete</i></button>';
    		})->editColumn('id', '{{$id}}')->make(true);
    }

    public function add(Request $req)
    {
    	if ($req->isMethod("post")) {
    		Murid::create([
                "firstname" => $req->firstname,
                "lastname" => $req->lastname
            ]);
    	}
    }

    public function edit(Request $req, $id)
    {
    	if ($req->isMethod("post")) {
    		
    	}
    }

    public function delete($id)
    {
        Murid::find($id)->delete();
    }
}
