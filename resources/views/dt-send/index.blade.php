@extends("layouts.app")

@section("header_title", "dt send")

@push("style")
    {!! Html::style("css/dataTables.bootstrap.min.css") !!}
    {!! Html::style("vendor/css/select2.min.css") !!}
@endpush

@section("contentheader_title", "DT SEND")

@section("content")

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover" id="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Tugas</th>
                                <th>Murid</th>
                                <th>Dekripsi</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push("script")
    {!! Html::script("js/jquery.dataTables.min.js") !!}
    {!! Html::script("js/dataTables.bootstrap.min.js") !!}
    {!! Html::script("vendor/js/select2.min.js") !!}
    {!! Html::script("js/fn.js") !!}
    {!! Html::script("js/event.js") !!}
    <script type="text/javascript">
    $(function() {
        var datatable = $('#table');

        datatable.DataTable({
            drawCallback: function() {
                 $('.select2-multi').select2();
              },
            processing: true,
            serverSide: true,
            responsive: true,
            language: {
              emptyTable: "Belum Ada Data"
            },
            search: { caseInsensitive: true },
            ajax: '{!! route('dt_send.data') !!}',
            columns: [
                { 
                    data: 'id', 
                    name: 'id',
                    render: function(data) {
                        return +data+'<input name="id" type="hidden" value="'+data+'" />';
                    },
                    searchable: false,
                    orderable: false
                },
                {
                    data: 'name', name: 'name'
                },
                { 
                    data: 'action', 
                    name: 'action',
                    searchable: false,
                    orderable: false
                },
                {
                    data: null,
                    render: function(data)
                    {
                        return '<input name="deskripsi" type="text" class="form-control" />';
                    },
                    searchable: false,
                    orderable: false
                },
                {
                    data: null,
                    render: function(data)
                    {
                        return '<button class="btn btn-primary waves-effect" id="send"><i class="material-icons">send</i>  Send</button>';
                    },
                    searchable: false,
                    orderable: false
                }
            ]
        });
        datatable.on("draw.dt", function() {
        fn.event.trigger("*", datatable);
        //extend form
        $.extend({
            redirectPost: function(location, data)
            {
                var form = '<input type="hidden" name="is_nama" value="'+data.murid+'"><input type="hidden" name="deskripsi" value="'+data.deskripsi+'">';
                var body = $('<form action="'+location+'" method="POST">{{ csrf_field() }}'+form+'</form>');
                $('body').append(body);
                body.submit();
            }
        });
           //setelah di submit
          $( document ).on("click", "#send", function(e){
            $('input[name="id"], input[name="deskripsi"] #nama');
              var x = $(this);
              var  data = {
                murid: x.parentsUntil("tr").parent().find(":selected").val(),
                deskripsi: x.parentsUntil("tr").parent().find("[name='deskripsi']").val(),
              };//
              var  id = x.parentsUntil("tr").parent().find("[name='id']").val();//tugas

              console.log(data.murid,id)
              $.ajax({
                url: '{{ route("dt_send.send", '') }}/'+id+'',
                success: function() {
                    console.log("success")
                  $.redirectPost("{{ route("dt_send.send", '') }}/"+id+"", data);
                }
              });
            e.preventDefault();
        });
      });
    });
    </script>
@endpush

