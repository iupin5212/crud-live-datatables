@extends("layouts.app")

@section("header_title", "DT AJAX CRUD DATATABLES")

@push("style")
    {!! Html::style("css/dataTables.bootstrap.min.css") !!}
    {!! Html::style("https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css") !!}
@endpush

@section("contentheader_title", "DT AJAX CRUD DATATABLES")

@section("content")

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="#" id="add">Tambah</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Firstname</th>
                                            <th>Lastname</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

@endsection

@push("script")
    {!! Html::script("js/jquery.dataTables.min.js") !!}
    {!! Html::script("js/dataTables.bootstrap.min.js") !!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js") !!}
        
    <script type="text/javascript">
    $(function() {

        var table = $('#table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: '{!! route('dt_crud.data') !!}',
            columns: [
                { data: 'id', name: 'id',
                    render: function(data) {
                        return +data+'<input name="id" type="hidden" value="'+data+'" />';
                    },
                    searchable: false
                },
                { data: 'firstname', name: 'firstname' },
                { data: 'lastname', name: 'lastname' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });

        $('#add').click(function(){
           var html = '<tr>';
           html += '<td></td>'
           html += '<td contenteditable id="firstname"></td>';
           html += '<td contenteditable id="lastname"></td>';
           html += '<td><button type="button" name="insert" id="insert" class="btn btn-success waves-effect"><i class="material-icons">send</i></button></td>';
           html += '</tr>';
           $('#table tbody').prepend(html);
          });

        $(document).on('click', '#insert', function(){
           var firstname = $('#firstname').text();
           var lastname = $('#lastname').text();
           if(firstname != null && lastname != null) {
                $.ajax({
                    url: "{{ route("dt_crud.add") }}",
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    method: "POST",
                    data:{ firstname: firstname, lastname: lastname },
                    success:function(data) {
                        sweetAlert("Yes!", "Your data has been submited", "success");
                        table.ajax.reload();
                    }
                });
            } else if(firstname == null && lastname == null) {
                sweetAlert("Whoops!", "All field are require", "error");
               }
            });

        $(document).on('click', '#delete', function(e){
            e.preventDefault();

            $('input[name="id"]');
            var x = $(this);
            var id = x.parentsUntil("tr").parent().find("[name='id']").val();

            swal({
                title: "Are you sure!",
                type: "error",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes!",
                showCancelButton: true,
            }, function() {
                $.ajax({
                 url:"{{ route("dt_crud.delete", "") }}/"+id+"",
                 method: "GET",
                 success:function(data){
                    sweetAlert("Yes!", "Your data has been deleted", "success");
                    table.ajax.reload();
                 }
                });
            });
        });

        $(document).on('click', '#update', function() {
            //some
        });
    });
    </script>
@endpush


