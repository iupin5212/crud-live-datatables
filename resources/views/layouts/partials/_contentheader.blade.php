<div class="block-header">
    <h2>
    	@yield('contentheader_title', '')
	    <small>@yield('contentheader_description')</small>
	</h2>
</div>