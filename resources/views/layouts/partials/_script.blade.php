{!! Html::script("vendor/plugins/jquery/jquery.min.js") !!}
{!! Html::script("vendor/plugins/bootstrap/js/bootstrap.js") !!}
{!! Html::script("vendor/plugins/jquery-slimscroll/jquery.slimscroll.js") !!}
{!! Html::script("vendor/plugins/node-waves/waves.js") !!}
@stack("script")
{!! Html::script("vendor/js/admin.js") !!}
{!! Html::script("vendor/js/demo.js") !!}
{!! Html::script("js/toastr.min.js") !!}
<script type="text/javascript">
  toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
  }

  //otomatis add class active pada menu sidebar
  var url = window.location;

  $('ul.list a').filter(function() {
   return this.href == url;
  }).parent().addClass('active');

  $('ul.ml-menu a').filter(function() {
   return this.href == url;
  }).parentsUntil(".list > .ml-menu").addClass('active');
</script>

<script>
  //toastr notikasi
  @if(Session::has('message'))
        var type="{{Session::get('alert-type','info')}}"


    switch(type){
        case 'info':
             toastr.info("{{ Session::get('message') }}");
             break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
        }
  @endif
</script>