<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Larabel | @yield("header_title", "YOUR TITLE")</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    {!! Html::style("https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext") !!}
    {!! Html::style("https://fonts.googleapis.com/icon?family=Material+Icons") !!}
    {!! Html::style("vendor/plugins/bootstrap/css/bootstrap.css") !!}
    {!! Html::style("vendor/plugins/node-waves/waves.css") !!}
    {!! Html::style("vendor/plugins/animate-css/animate.css") !!}
    {!! Html::style("css/toastr.min.css") !!}
    @stack("style")
    {!! Html::style("vendor/css/style.css") !!}
    {!! Html::style("vendor/css/themes/all-themes.css/") !!}
</head>