<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

@include("layouts.partials._head")

<body class="theme-teal">

    @include("layouts.partials._topbar")

    @include("layouts.partials._sidebar")

    <section class="content">
        <div class="container-fluid">
            @include("layouts.partials._contentheader")

            @yield("content")
        </div>
    </section>

    @include("layouts.partials._script")
</body>
</html>
