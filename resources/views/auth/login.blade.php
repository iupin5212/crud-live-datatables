<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Login Larabel</title>
    <!-- Favicon-->
    <link rel="icon" href="{{ asset("vendor/favicon.ico") }}" type="image/x-icon">
    {!! Html::style("https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext") !!}
    {!! Html::style("https://fonts.googleapis.com/icon?family=Material+Icons") !!}
    {!! Html::style("vendor/plugins/bootstrap/css/bootstrap.css") !!}
    {!! Html::style("vendor/plugins/node-waves/waves.css") !!}
    {!! Html::style("vendor/plugins/animate-css/animate.css") !!}
    {!! Html::style("vendor/css/style.css") !!}
    {!! Html::style("https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css") !!}
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);"><b>LARABEL</b></a>
            <small>ajax crud for datatables</small>
        </div>
        <div class="card">
            <div class="body">
                {!! Form::open(["route" => ["login"], "method" => "post", "id" => "sign_in"]) !!}
                    <div class="msg">   </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            {!! Form::text("email", null, ["class" => "form-control", "placeholder" => "Email", "required", "autofocus"]) !!}
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            {!! Form::password("password", ["class" => "form-control", "placeholder" => "Password", "required"]) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="remember"{{ old('remember') ? 'checked' : '' }} id="rememberme" class="filled-in chk-col-teal">
                            <label for="rememberme">Remember Me</label>
                        </div>
                        <div class="col-xs-4">
                            <button id="submit-btn" class="btn btn-block bg-teal waves-effect" type="submit">SIGN IN</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="{{ route("register") }}">Register Now!</a>
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="#">Forgot Password?</a>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    {!! Html::script("vendor/plugins/jquery/jquery.min.js") !!}
    {!! Html::script("vendor/plugins/bootstrap/js/bootstrap.js") !!}
    {!! Html::script("vendor/plugins/node-waves/waves.js") !!}
    {!! Html::script("vendor/plugins/jquery-validation/jquery.validate.js") !!}
    {!! Html::script("vendor/js/admin.js") !!}
    {!! Html::script("vendor/js/pages/examples/sign-in.js") !!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js") !!}
    @if (count($errors) > 0)
    <script>
        sweetAlert("Whoops!", "Email or password is incorrect", "error");
    </script>
    @endif
</body>

</html>