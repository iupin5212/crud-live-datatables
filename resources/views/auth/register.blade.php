<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign Up | Larabel</title>
    <link rel="icon" href="{{ asset("vendor/favicon.ico") }}" type="image/x-icon">
    {!! Html::style("https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext") !!}
    {!! Html::style("https://fonts.googleapis.com/icon?family=Material+Icons") !!}
    {!! Html::style("vendor/plugins/bootstrap/css/bootstrap.css") !!}
    {!! Html::style("vendor/plugins/node-waves/waves.css") !!}
    {!! Html::style("vendor/plugins/animate-css/animate.css") !!}
    {!! Html::style("vendor/css/style.css") !!}
    {!! Html::style("https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css") !!}
</head>

<body class="signup-page">
    <div class="signup-box">
        <div class="logo">
            <a href="javascript:void(0);"><b>LARABEL</b></a>
            <small>Ajax crud for datatables</small>
        </div>
        <div class="card">
            <div class="body">
                {!! Form::open(["route" => ["register"], "method" => "post", "id" => "sign_up"]) !!}
                    <div class="msg">Register a new membership</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            {!! Form::text("name", null, ["class" => "form-control", "placeholder" => "Name", "required", "autofocus"]) !!}
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            {!! Form::email("email", null, ["class" => "form-control", "placeholder" => "Email Address", "required"]) !!}
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            {!! Form::password("password", ["class" => "form-control", "minlength" => "6", "placeholder" => "Password", "required"]) !!}
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            {!! Form::password("password_confirmation", ["class" => "form-control", "minlength" => "6", "placeholder" => "Confirm Password", "required"]) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="terms" id="terms" class="filled-in chk-col-teal">
                        <label for="terms">I read and agree to the <a href="javascript:void(0);">terms of usage</a>.</label>
                    </div>

                    <button class="btn btn-block btn-lg bg-teal waves-effect" type="submit">SIGN UP</button>

                    <div class="m-t-25 m-b--5 align-center">
                        <a href="{{ url("/login") }}">You already have a membership?</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    {!! Html::script("vendor/plugins/jquery/jquery.min.js") !!}
    {!! Html::script("vendor/plugins/bootstrap/js/bootstrap.js") !!}
    {!! Html::script("vendor/plugins/node-waves/waves.js") !!}
    {!! Html::script("vendor/plugins/jquery-validation/jquery.validate.js") !!}
    {!! Html::script("vendor/js/admin.js") !!}
    {!! Html::script("vendor/js/pages/examples/sign-up.js") !!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js") !!}
    @if ($errors == null)
    <script>
        sweetAlert("Registered!", "Please Login", "success");
    </script>
    @elseif ($errors->has("name"))
    <script>
        sweetAlert("Whoops!", "{{ $errors->first("name") }}", "error");
    </script>
    @elseif ($errors->has("email"))
    <script>
        sweetAlert("Whoops!", "{{ $errors->first("email") }}", "error");
    </script>
    @elseif ($errors->has("password"))
    <script>
        sweetAlert("Whoops!", "{{ $errors->first("password") }}", "error");
    </script>
    @endif
</body>

</html>
