<?php

Route::get('/', function () {
    return redirect("/login");
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {

	Route::group(["prefix" => "datatables-send"], function (){
		Route::get("/", "DatatablesController@index")->name("dt_send.index");
		Route::any("data", "DatatablesController@data")->name("dt_send.data");
		Route::match(["get", "post"], "send/{id}", "DatatablesController@send")->name("dt_send.send");
	});

	Route::group(["prefix" => "datatables-crud-ajax"], function (){
		Route::get("/", "DatatablesController@index2")->name("dt_crud.index");
		Route::any("data", "DatatablesController@data2")->name("dt_crud.data");
		Route::match(["get", "post"], "add", "DatatablesController@add")->name("dt_crud.add");
		Route::get("delete/{id}", "DatatablesController@delete")->name("dt_crud.delete");
	});

});